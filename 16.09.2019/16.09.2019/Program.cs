﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16._09._2019
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите число");
            string n = Console.ReadLine();
            System.Int32 a = int.Parse(n);
            if ((a % 2 == 0) && (Math.Abs(a) >99) && (Math.Abs(a)<1000) )
                    {
                Console.WriteLine("да");
            }
            else
            {
                Console.WriteLine("нет");
            }
            Console.ReadKey();

            //1
            Console.WriteLine("Введите два числа");
            string[] ch = Console.ReadLine().Split(' ', ',');
            if (int.Parse(ch[0]) >= int.Parse(ch[1]))
            {
                Console.WriteLine(ch[0]);
            }
            else
            {
                Console.WriteLine(ch[1]);
            }
            Console.ReadKey();

            //2
            Console.WriteLine("Введите два числа");
            string[] ch2 = Console.ReadLine().Split(' ', ',');
            if (int.Parse(ch2[0]) > int.Parse(ch2[1]))
            {
                Console.WriteLine(1);
            }
            else
            {
                if (int.Parse(ch2[0]) < int.Parse(ch2[1]))
                {
                    Console.WriteLine(2);
                }
                else
                {
                    Console.WriteLine(0);
                }
            }
            Console.ReadKey();

            //4
            Console.WriteLine("Введите три числа");
            string[] ch3 = Console.ReadLine().Split(' ', ',');
            if ((int.Parse(ch3[0]) == int.Parse(ch3[1]))&& (int.Parse(ch3[1]) == int.Parse(ch3[2])))
            {
                Console.WriteLine(3);
            }
            else
            {
                if ((int.Parse(ch3[0]) == int.Parse(ch3[1]))|| (int.Parse(ch3[1]) == int.Parse(ch3[2]))|| (int.Parse(ch3[0]) == int.Parse(ch3[2])))
                {
                    Console.WriteLine(2);
                }
                else
                {
                    Console.WriteLine(1);
                }
            }
            Console.ReadKey();

            //5
            Console.WriteLine("Введите координаты двух клеток");
            string[] c = Console.ReadLine().Split(' ', ',');
            if ((int.Parse(c[0]) % 10 == int.Parse(c[1]) % 10) || (int.Parse(c[0]) / 10 == int.Parse(c[1]) / 10))
            {
                Console.WriteLine("YES");
            }
            else
                Console.WriteLine("NO");
            Console.ReadKey();

        }
    }
}
